#pragma once

#include <png.h>
#include <iostream>

struct RGB {
    int r, g, b;
    bool operator==(const RGB& rhs) const {
        return r == rhs.r && g == rhs.g && b == rhs.b;
    }
};

std::ostream& operator<<(std::ostream& out, const RGB& x) {
    out << x.r << " " << x.g << " " << x.b;
    return out;
}

class Image {
public:
    explicit Image(const std::string& filename) {
    }

    void Write(const std::string& filename) {
    }

    RGB GetPixel(int y, int x) const {
    }

    void SetPixel(const RGB& pixel, int y, int x) {
    }

    int Height() const {
        return height_;
    }

    int Width() const {
        return width_;
    }

private:
    int width_, height_;
};
