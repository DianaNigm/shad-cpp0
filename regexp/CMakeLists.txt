find_path(RE2_INCLUDE_DIRS NAMES re2/re2.h HINTS /usr/include /usr/local/include)
find_library(RE2_LIBRARIES NAMES re2 HINTS /usr/lib /usr/local/lib)

if (RE2_INCLUDE_DIRS AND RE2_LIBRARIES)
    message(STATUS "Found RE2: ${RE2_INCLUDE_DIRS}")
    include_directories(${RE2_INCLUDE_DIRS})

    add_catch(test_regexp test.cpp)
    target_link_libraries(test_regexp ${RE2_LIBRARIES} pthread)
else()
    message(WARNING "Could not find RE2 library.")
endif()

