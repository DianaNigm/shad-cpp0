#pragma once

#include <memory>

#include <tokenizer.h>

class Object {
public:
    virtual ~Object() = default;
};

struct SyntaxError : public std::runtime_error {
    explicit SyntaxError(const std::string& what) : std::runtime_error(what) {
    }
};

bool IsNumber(const std::shared_ptr<Object>& obj);
TODO AsNumber(const std::shared_ptr<Object>& obj);

bool IsCell(const std::shared_ptr<Object>& obj);
TODO AsCell(const std::shared_ptr<Object>& obj);

bool IsSymbol(const std::shared_ptr<Object>& obj);
TODO AsSymbol(const std::shared_ptr<Object>& obj);

std::shared_ptr<Object> Read(Tokenizer* tokenizer);