cmake_minimum_required(VERSION 2.8)
project(polynom)

if (TEST_SOLUTION)
  include_directories(../private/polynom)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_polynom test.cpp)
